--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.15
-- Dumped by pg_dump version 9.6.15

-- Started on 2019-08-28 22:08:17 NZST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4456 (class 0 OID 22968)
-- Dependencies: 213
-- Data for Name: address; Type: TABLE DATA; Schema: address; Owner: postgres
--

--
-- Data for Name: address; Type: TABLE DATA; Schema: address; Owner: postgres
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE address DISABLE TRIGGER ALL;

ALTER TABLE address ENABLE TRIGGER ALL;


-- Completed on 2019-08-28 22:08:17 NZST

--
-- PostgreSQL database dump complete
--

